import { TabsPage } from './pages/tabs/tabs';
import { HomePage } from './pages/home/home.page';

export namespace Settings {

    export const firebaseConfig = {
        apiKey: "AIzaSyDpzGmOWmKa0-FhvTEoEjLttBjo_W4r4jc",
        authDomain: "chat-module-990a9.firebaseapp.com",
        databaseURL: "https://chat-module-990a9.firebaseio.com",
        projectId: "chat-module-990a9",
        storageBucket: "chat-module-990a9.appspot.com",
        messagingSenderId: "1073706588865"
    };

    export const facebookLoginEnabled = true;
    export const googleLoginEnabled = true;
    export const phoneLoginEnabled = true;

    export const facebookAppId: string = "178255756309984";
    export const googleClientId: string = "1073706588865-7rmamr8urgsuacoqfgs30dld3q1bu50b.apps.googleusercontent.com";
    export const customTokenUrl: string = "https://us-central1-chat-module-990a9.cloudfunctions.net/getCustomToken";
    export const fcm_api_key :string = "key=AAAA-f3mWsE:APA91bEP4oLh_QKJFNFrr5D199vAAZICcbyR8wErw9ZHUxtDf-wi569QaTg1qIj1peqDa-8TZOuAWFa1QC9OWtyZSAxj3IS6HiAmw-h-RJVS6XjvpkxmgJ46nMiHVHa4h3P73D_2BYwc";
    export const fcm_api_url:string="https://fcm.googleapis.com/fcm/send"  ;
    export const homePage = HomePage;


}