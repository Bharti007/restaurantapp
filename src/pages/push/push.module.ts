import { NgModule } from '@angular/core';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { IonicModule, Platform } from 'ionic-angular';
import { sender_id } from '../../config';

@NgModule({
	imports: [IonicModule],
	declarations: [],
	entryComponents: [],
	providers: [
		Push
	]
})
export class PushModule {
	constructor(private platform: Platform, private push: Push) {
		this.platform.ready().then(() => {
			if (!this.platform.is('cordova')) {
				return;
			}
			// to check if we have permission
			this.push.hasPermission()
				.then((res: any) => {

					if (res.isEnabled) {
						console.log('We have permission to send push notifications');
					} else {
						console.log('We do not have permission to send push notifications');
					}

				});

			// to initialize push notifications
			const options: PushOptions = {
				android: {
					senderID: sender_id
				},
				ios: {
					alert: 'true',
					badge: true,
					sound: 'false'
				},
				windows: {}
			};

			const pushObject: PushObject = this.push.init(options);

			pushObject.on('notification')
				.subscribe((notification: any) => {
					console.log('Received a notification', notification);
					alert('Received a notification');
				});

			pushObject.on('registration')
				.subscribe((registration: any) => {
					console.log('Device registered', registration);
					alert('Device registered');
				});

			pushObject.on('error')
				.subscribe(error => {
					console.error('Error with Push plugin', error);
					alert('Error with Push plugin');
				});
		});
	}
}
