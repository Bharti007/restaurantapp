import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { InAppBrowserService } from '../../../services/in-app-browser.service';

@Component({
	selector: 'news-item',
	templateUrl: 'news.item.html'
})
export class NewsItemPage {
	picture: string;
	post: any;

	constructor(navParams: NavParams, private inAppBrowser: InAppBrowserService) {
		this.post = navParams.get('item');
	}

	openLink(url: string) {
		this.inAppBrowser.open(url);
	}
}
