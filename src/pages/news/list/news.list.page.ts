import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsService } from '../news.service';
import { NewsItemPage } from '../item/news.item.page';

import { AngularFirestoreModule, AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';

@Component({
	templateUrl: 'news.list.html',
	providers: [NewsService]
})
export class NewsListPage implements OnInit {
	private service: NewsService;
	private nav: NavController;
	public posts: any[];
	public postcollection:any;

	constructor(service: NewsService, nav: NavController , public afstore: AngularFirestore,) {
		this.service = service;
		this.nav = nav;
        this.postcollection = this.afstore.collection('posts');
	}

	ngOnInit(): void {

        this.postcollection.valueChanges().subscribe(posts=>{
        	console.log(posts)
            this.posts = posts;
		}
	)


            // .map(actions => {
            //     return actions.map(a => {
            //         const data = a.payload.doc.data() as posts;
            //         data['id'] = a.payload.doc.id;
            //         return { data };
            //     })
            // })
		// this.service.getPosts()
		// 	.subscribe(posts => {
		// 		this.posts = posts;
		// 		console.log(posts);
		// 	});
	}

	public itemTapped(item) {
		this.nav.push(NewsItemPage, {
			item: item
		});
	}

    public doRefresh(refresher) {
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
}
