import { Component } from '@angular/core';
import {App, Nav} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { MapPage } from '../map/map.page';
import { Tile } from './models/tile.model';
import { CallService } from '../../services/call.service';
import { MapsService } from '../../services/maps.service';
import { AlbumsListPage } from '../albums/albums-list.page';

import { EventsListPage } from "../events/events-list.page";
import { EventsPage } from '../calender/events.page';
import { InAppBrowserService } from '../../services/in-app-browser.service';
import { NewsListPage } from '../news/list/news.list.page';
import { ContactUsPage } from './contact-us.page';
import { PageInfoService } from './page-info.service';
import { EmailComposer } from '@ionic-native/email-composer';
import { MessagesPage } from '../messages/messages';

import { DataProvider } from '../../providers/data';
import { LoadingProvider } from '../../providers/loading';
import { GroupPage } from '../group/group';

import{MenuItemsPage } from '../menu-items/menu-items.page'
@Component({
	templateUrl: 'home.html'
})
export class HomePage {
	tiles: Tile[][];
	data: any = {};
	status:boolean=true;
	friendlyActions = {
		'WEBSITE': 'Visit Website',
		'PHONE_CALL': 'Phone Call',
		'EMAIL': 'Send Email'
	};
    private groups: any=[];
    private searchGroup: any;
    private updateDateTime: any;
	constructor(
		private callService: CallService,
		private mapsService: MapsService,
		private browserService: InAppBrowserService,
		private contactUsService: PageInfoService,
		private nav: Nav,
		private emailComposer: EmailComposer,
        public dataProvider: DataProvider,
		public loadingProvider: LoadingProvider,
        public app: App
	) {

       // this.fire.sendPushNotification("Friend Request",+"hartib has sent you friend request.","eHgpHmotDbU:APA91bG0kVSsGMrdco_Mg2uH2jR-t5qMKNIWN12QslLJvC8P1DOGyf6JABBo7lZ0f1gBDAPJ7sgu9j2zUTWxjbpFBEeKvVfI09fx492Z1NRgZna7ldJ_yioCnczLHMY_6bG5N80FWV8l");
		
	}

    ionViewDidLoad() {
        // Initialize
        this.searchGroup = '';
        this.loadingProvider.show();

        // Get groups
        this.dataProvider.getGroups().snapshotChanges().subscribe((groupIdsRes) => {
            let groupIds = [];
            groupIds = groupIdsRes.map(c => ({ key: c.key, ...c.payload.val()}));
            console.log(groupIds);
            if (groupIds.length > 0) {
                if(this.groups && this.groups.length > groupIds.length) {
                    // User left/deleted a group, clear the list and add or update each group again.
                    this.groups = [];
                }
                groupIds.forEach((groupId) => {
                    console.log(groupId);
                    this.dataProvider.getGroup(groupId.key).snapshotChanges().subscribe((groupRes) => {
                        let group = { key: groupRes.key, ...groupRes.payload.val() };
                        console.log(group);

                        if (group.key != null) {

                            // Get group's unreadMessagesCount
                            group.unreadMessagesCount = group.messages.length - groupId.messagesRead;
                            // Get group's last active date
                            group.date = group.messages[group.messages.length - 1].date;
                            this.addOrUpdateGroup(group);
                        }

                    });
                });
                this.loadingProvider.hide();
            } else {
                this.groups = [];
                this.loadingProvider.hide();
            }
        });

        // Update groups' last active date time elapsed every minute based on Moment.js.
        var that = this;
        if (!that.updateDateTime) {
            that.updateDateTime = setInterval(function() {
                if (that.groups) {
                    that.groups.forEach((group) => {
                        let date = group.date;
                        group.date = new Date(date);
                    });
                }
            }, 60000);
        }
    }

	ngOnInit(): any {
		this.contactUsService.getInfo()
			.subscribe(info => {
				this.data = info;
				console.log(info);
			});

		this.initTiles();
	}

	

	callToAction() {
		let callToActions = this.data.callToActions;

		switch (callToActions.web_destination_type) {
			case 'WEBSITE':
				this.browserService.open(callToActions.web_url);
				break;
			case 'PHONE_CALL':
				this.callService.call(callToActions.intl_number_with_plus);
				break;
			case 'EMAIL':
				let email = {
					to: callToActions.email_address,
					subject: 'A need your help',
					body: 'I have a question'
				};

				this.emailComposer.open(email);
				break;
		}
	}

	navigateTo(tile,action) {

		   this.nav.push(tile.component);

	}

	getDirections() {
		this.mapsService.openMapsApp(this.data.location.latitude + ',' + this.data.location.longitude, this.data.name);
	}

	sendEmail() {
		let email = {
			to: this.data.email,
			subject: 'A need your help',
			body: 'I have a question'
		};

		this.emailComposer.open(email);
	}

	openWebsite() {
		this.browserService.open(this.data.website);
	}

	callUs() {
		this.callService.call(this.data.phone);
	}

	showMore() {
		this.nav.push(ContactUsPage);
	}

	private initTiles(): void {
		this.tiles = [[
			{
			title: 'Menu',
			path: 'chat',
			icon: 'list-box',
			image: 'assets/img/youth.png',
			component: MenuItemsPage,
		},{
			title: 'News',
			path: 'news',
			icon: 'paper',
			image: 'assets/img/event.png',
			component: NewsListPage
		},{
                title: 'Events',
                path: 'news',
                icon: 'calendar',
                image: 'assets/img/event.png',
                component: EventsPage
            },{
                title: 'My Music',
                path: 'news',
                icon: 'play',
                image: 'assets/img/event.png',
                component: NewsListPage
            }

	
	]];
	}
	clickEvent(){
		this.status=!this.status
	}



    // Add or update group for real-time sync based on our observer.
    addOrUpdateGroup(group) {
        if (!this.groups) {
            this.groups = [group];
        } else {
            var index = -1;
            for (var i = 0; i < this.groups.length; i++) {
                if (this.groups[i].key == group.key) {
                    index = i;
                }
            }
            if (index > -1) {
                this.groups[index] = group;
            } else {
                this.groups.push(group);
            }
        }
    }

    // Open Group Chat.
    viewGroup(groupId) {
        console.log(groupId)
        this.app.getRootNav().push(GroupPage, { groupId: groupId });
    }

	
}
