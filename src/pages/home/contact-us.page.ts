import { Component, OnInit } from '@angular/core';
import { InAppBrowserService } from '../../services/in-app-browser.service';
import { PageInfoService } from './page-info.service';

@Component({
	templateUrl: 'contact-us.html'
})
export class ContactUsPage implements OnInit {
	coverPicture: string;
	address: string;
	description: string;
	name: string;

	constructor(
		private infoService: PageInfoService,
		private browserService: InAppBrowserService
	) {
		this.infoService = infoService;
		this.browserService = browserService;
	}

	ngOnInit(): any {
		this.infoService.getInfo()
			.subscribe(info => {
				this.coverPicture = info.cover.source;
				this.description = info.description;
				this.name = info.name;
				this.address = info.address;
			});
	}
}
