import { Component } from '@angular/core';
 import { CalendarDateFormatter } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { Observable } from 'rxjs/Observable';
import {FacebookApiService} from "../../services/facebook-api.service";
import { CustomDateFormatter } from './custom-date-formatter';

@Component({
	selector: 'events',
	templateUrl: './events.page.html',
    providers: [{
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter
    }]

})
export class EventsPage {
	eventt: any=[];
	viewDate: Date = new Date();
	view = 'month';
	activeDayIsOpen: boolean = false;

	events: any[] = [];

	constructor(dataService: FacebookApiService) {
		setTimeout(function () {
            this.eventt = dataService.getEvents();
            console.log(this.eventt)
        },5000)

	}

	dayClicked({ date, events }: { date: Date, events: any[] }): void {

		if (isSameMonth(date, this.viewDate)) {
			if ((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0) {
				this.activeDayIsOpen = false;
			} else {
				this.activeDayIsOpen = true;
				this.viewDate = date;
			}
		}
	}

	viewDateChange() {
		this.activeDayIsOpen = false;
	}
}
