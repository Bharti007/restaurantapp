import { NgModule } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { ReviewsPage } from './reviews.page';
import { Ionic2RatingModule } from 'ionic2-rating';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
	declarations: [ReviewsPage],
	entryComponents: [ReviewsPage],
	imports: [IonicModule, Ionic2RatingModule, PipesModule]
})
export class ReviewsModule {

}