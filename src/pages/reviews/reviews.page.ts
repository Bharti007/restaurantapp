import { Component, OnInit } from '@angular/core';
import { FacebookApiService } from '../../services/facebook-api.service';

@Component({
	templateUrl: 'reviews.page.html'
})
export class ReviewsPage implements OnInit {
	reviews: any[];

	constructor(private service: FacebookApiService) {
	}

	ngOnInit(): any {
		this.service.getReviews()
			.subscribe(reviews => {
				this.reviews = reviews;
			});
	}
}
