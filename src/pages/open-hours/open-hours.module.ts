import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { OpenHoursPage } from './open-hours.page';

@NgModule({
	declarations: [OpenHoursPage],
	entryComponents: [OpenHoursPage],
	imports: [IonicModule]
})
export class OpenHoursModule {

}