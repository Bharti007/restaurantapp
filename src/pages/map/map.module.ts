import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { MapPage } from './map.page';
import { AgmCoreModule } from '@agm/core';
import { MapService } from './map.service';

@NgModule({
	declarations: [MapPage],
	entryComponents: [MapPage],
	imports: [IonicModule, AgmCoreModule],
	providers: [MapService]
})
export class MapModule {

}