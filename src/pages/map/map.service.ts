import { Injectable } from '@angular/core';
import { FacebookApiService } from '../../services/facebook-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class MapService {
	private service: FacebookApiService;

	constructor(service: FacebookApiService) {
		this.service = service;

	}

	getLocation(): Observable<any> {
		return this.service.getInfo()
			.map(x => ({
				location: x.location,
				name: x.name
			}));
	}
}