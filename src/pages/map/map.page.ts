import { Component, OnInit } from '@angular/core';
import { Marker, Point } from './interfaces';
import { MapService } from './map.service';

@Component({
	templateUrl: 'map.html'
})
export class MapPage implements OnInit {
	markers: Marker[] = [];
	origin: Point;
	zoom = 8;

	private service: MapService;

	constructor(service: MapService) {
		this.service = service;
	}

	public ngOnInit(): void {
		this.service.getLocation()
			.subscribe(x => {
				this.markers = [{
					lat: x.location.latitude,
					lng: x.location.longitude,
					label: x.name
				}];
				this.origin = {
					lat: x.location.latitude,
					lng: x.location.longitude
				};
			});
	}
}