import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { AlbumDetailsPage } from './album-details.page';
import { AlbumsListPage } from './albums-list.page';
import { AlbumsService } from './albums.service';

@NgModule({
	imports: [IonicModule, PipesModule],
	declarations: [
		AlbumDetailsPage,
		AlbumsListPage
	],
	entryComponents: [
		AlbumDetailsPage,
		AlbumsListPage
	],
	providers: [AlbumsService]
})
export class AlbumsModule {

}