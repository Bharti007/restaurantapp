import { Component, OnInit } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AlbumsService } from './albums.service';

@Component({
	selector: 'album-details',
	templateUrl: 'album-details.page.html'
})
export class AlbumDetailsPage implements OnInit {
	pictures: any[];
	album: any;

	constructor(navParams: NavParams, private service: AlbumsService) {
		this.album = navParams.get('item');
	}

	ngOnInit(): void {
		this.service.getAlbum(this.album.id)
			.subscribe(pictures => {
				this.pictures = pictures;
				console.log(pictures);
			});
	}
}
