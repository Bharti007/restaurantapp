import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlbumsService } from './albums.service';
import { AlbumDetailsPage } from './album-details.page';

@Component({
	selector: 'albums-list',
	templateUrl: 'albums-list.page.html'
})
export class AlbumsListPage implements OnInit {
	private service: AlbumsService;
	private nav: NavController;
	public albums: any[];

	constructor(service: AlbumsService, nav: NavController) {
		this.service = service;
		this.nav = nav;
	}

	ngOnInit(): void {
		this.service.getAlbums()
			.subscribe(albums => {
				this.albums = albums;
				console.log(albums);
			});
	}

	public itemTapped(item) {
		this.nav.push(AlbumDetailsPage, {
			item: item
		});
	}
}
