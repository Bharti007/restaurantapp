import { Injectable } from '@angular/core';
import { FacebookApiService } from '../../services/facebook-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class AlbumsService {
	private facebookApiService: FacebookApiService;

	constructor(facebookApiService: FacebookApiService) {
		this.facebookApiService = facebookApiService
	}

	public getAlbums(): Observable<any[]> {
		return this.facebookApiService.getAlbums();
	}

	getAlbum(id) {
		return this.facebookApiService.getAlbum(id);
	}
}