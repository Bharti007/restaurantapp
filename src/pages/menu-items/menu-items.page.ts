import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { MenuItemPage } from './manu-item.page';

import {FacebookApiService} from "../../services/facebook-api.service";

@Component({
	templateUrl: './menu-items.page.html',
	selector: 'as-page-menu-items'
})
export class MenuItemsPage {
	items: any=[];

	constructor(private menudata: FacebookApiService, private nav: NavController) {
		this.items = menudata.getMenuItems();

    }

	public itemTapped(item) {
		this.nav.push(MenuItemPage, {
			item: item
		});
	}
}
