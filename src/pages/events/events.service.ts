import { Injectable } from '@angular/core';
import { FacebookApiService } from '../../services/facebook-api.service';
import { Observable } from 'rxjs';

@Injectable()
export class EventsService {
	private facebookApiService: FacebookApiService;

	constructor(facebookApiService: FacebookApiService) {
		this.facebookApiService = facebookApiService
	}

	public getEvents(): Observable<any[]> {
		return this.facebookApiService.getEvents();
	}
}