import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../../pipes/pipes.module';
import { EventDetailsPage } from './event-details.page';
import { EventsListPage } from './events-list.page';
import { EventsService } from './events.service';

@NgModule({
	imports: [IonicModule, PipesModule],
	declarations: [
		EventDetailsPage,
		EventsListPage
	],
	entryComponents: [
		EventDetailsPage,
		EventsListPage
	],
	providers: [EventsService]
})
export class EventsModule {

}