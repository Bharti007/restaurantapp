import { NgModule } from '@angular/core';
import { TrimHtmlPipe } from './trim-html.pipe';
import { TruncatePipe } from './truncate.pipe';
import { LocalDatePipe } from './local-date.pipe';
import { StringToDatePipe } from './string-to-date.pipe';

@NgModule({
	declarations: [
		TruncatePipe,
		TrimHtmlPipe,
		LocalDatePipe,
		StringToDatePipe
	],
	exports: [
		TruncatePipe,
		TrimHtmlPipe,
		LocalDatePipe,
		StringToDatePipe
	]
})
export class PipesModule {

}