import { Injectable } from '@angular/core';
import { Config } from '../config';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';

@Injectable()
export class FacebookApiService {
	private http: Http;
	private pageAccessToken: string;
	public menuList :any=[];
	public events:any=[];

	constructor(http: Http) {
		this.http = http;
	}

	getInfo(): Observable<any> {
		let fields = 'category,hours,cover,emails,location,phone,username,id,name,about,description,website,access_token,';
		fields += 'call_to_actions{email_address,intl_number_with_plus,web_destination_type,destination_type,web_url}';
		return this.http.get(Config.apiUrl + Config.pageName, this.options(fields))
			.map(x => x.json())
			.do(info => {
				console.log(info);

				if (info != undefined && info != '') {
					info.location = {
						"city": "Sydney",
						"country": "Australia",
						"latitude": -33.884609,
						"longitude": 151.196269,
						"state": "AU",
						"street": "166 Broadway, Chippendale",
						"zip": "NSW 2008"
					}
					info.cover.source = "assets/img/charity.jpg"
					info.phone = "(02) 9211 5993";
					info.email = "info@broadwaycrown.com.au";
					info.name = "Broadway Crown"
					info.website = "http://www.broadwaycrown.com.au/";
					info.description = "Step into the action at Broadway Crown, formally known as The Broadway Hotel, our lavish art deco venue offers you the perfect setting for after work drinks, cocktails, pizzas, and burgers. Step into the action at Broadway Crown, formally known as The Broadway Hotel, our lavish art deco venue offers you the perfect setting for after work drinks, cocktails, pizzas, and burgers. Allow the ambiance to captivate you, pull up a pew and immerse yourself in hand picked craft beers, signature cocktails and a food menu to suit all tastes offering you pizzas, burgers and classic pub dishes.Located within the hustle of Broadway Chippendale, Sydney - Broadway Crown is picture perfect."
				}

				this.pageAccessToken = info.access_token;
			});
	}

	// getEvents() {
	// 	let fields = 'description,name,place,start_time,end_time,cover,category';
	// 	return this.http.get(Config.apiUrl + Config.pageName + '/events', this.options(fields))
	// 		.map(x => x.json())
	// 		.map(result => {
	// 			return result.data;
	// 		});
	// }

	getAlbums() {
		let fields = 'cover_photo,name,description';
		return this.http.get(Config.apiUrl + Config.pageName + '/albums', this.options(fields))
			.map(x => x.json())
			.map(result => {
				var albums = [];
				result.data.forEach(album => {
					let coverPhoto = album['cover_photo'];
					if (coverPhoto) {
						let cover = `${Config.apiUrl}${coverPhoto.id}/picture?access_token=${Config.facebookPermanentAccessToken}`;
						albums.push({
							id: album.id,
							cover: cover,
							title: album.name,
							description: album.description
						});
					}
				});

				return albums;
			});
	}

	getAlbum(albumId: string): Observable<any> {
		let fields = 'name,place,picture,images,created_time';
		let url = Config.apiUrl + albumId + '/photos';
		return this.http.get(url, this.options(fields))
			.map(x => x.json())
			.map(x => x.data);
	}

	getPosts(): Observable<any> {
		let fields = 'posts{created_time,description,picture,full_picture,caption,message,story,type,object_id,link,from,name,place}';
		return this.http.get(Config.apiUrl + Config.pageName, this.options(fields))
			.map(x => x.json())
			.map((x: any) => {
				return x.posts.data;
			});
	}

	getReviews() {
		let fields = 'review_text,created_time,rating,reviewer,has_rating,has_review';
		return this.http.get(`${Config.apiUrl + Config.pageName}/ratings`, this.options(fields, this.pageAccessToken))
			.map(x => x.json())
			.map((x: any) => {
				return x.data;
			});
	}

	private options(fields: string = null, accessToken?: string) {
		let params = new URLSearchParams();
		params.set('access_token', accessToken || Config.facebookPermanentAccessToken);
		if (fields) {
			params.set('fields', fields);
		}
		return new RequestOptions({
			search: params,

		});
	}

    getMenuItems(){
		this.menuList = [
            {
                "body": "Donec et quam ultrices, maximus urna quis, elementum neque. Phasellus eu ultrices odio, in venenatis erat. Curabitur feugiat ut tellus non feugiat.",
                "category": "Appetizers",
                "isFeatured": false,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/207a9a5e-e8e0-3ad8-1550-0d3cdd490031-menu-item1.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F207a9a5e-e8e0-3ad8-1550-0d3cdd490031-menu-item1.png?alt=media&token=2d7bfab8-0600-43d4-8187-66c3a9594ffc"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Standard",
                        "value": "13.59"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Tomato sauce",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/766152f5-1368-5376-e61c-ab8a9961e603-menu-item1-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F766152f5-1368-5376-e61c-ab8a9961e603-menu-item1-thumb.png?alt=media&token=ca6ab732-ba1d-4909-82da-4cb3b3407152"
                    }
                ],
                "title": "Cauliflower"
            },
            {
                "body": "Maecenas magna lacus, scelerisque vitae interdum nec, imperdiet bibendum risus. Curabitur et pretium leo, dignissim pellentesque dui.",
                "category": "Main Course",
                "extraOptions": [
                    {
                        "deliveryDate": 1503638821000,
                        "name": "Dill Sauce",
                        "selected": false,
                        "value": 3.15
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/de338ecc-2083-0308-b4a8-1003c9528fcb-menu-item2-2.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2Fde338ecc-2083-0308-b4a8-1003c9528fcb-menu-item2-2.jpeg?alt=media&token=d59ed716-0a9c-425c-a594-b649d2264e0c"
                    },
                    {
                        "inProgress": false,
                        "path": "files/4603ac58-51b9-26ec-23df-994efa3b195b-menu-item2-1.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F4603ac58-51b9-26ec-23df-994efa3b195b-menu-item2-1.jpeg?alt=media&token=345d4678-3094-4853-b805-66636b93965f"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Single",
                        "value": 8
                    },
                    {
                        "currency": "$",
                        "name": "Double",
                        "value": 13
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Rosemary Leaves",
                        "selected": true
                    },
                    {
                        "name": "Steamed Asparagus",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/3b740454-1b5d-b097-90b5-c5d9f3ca2270-menu-item2-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F3b740454-1b5d-b097-90b5-c5d9f3ca2270-menu-item2-thumb.png?alt=media&token=b60d1747-34ab-4234-980b-595b6c55949d"
                    }
                ],
                "title": "Salmon Fillet"
            },
            {
                "body": "Donec euismod mattis finibus. In finibus risus vitae tortor varius, eu ornare erat sodales. Nullam ut dignissim mauris.",
                "category": "Appetizers",
                "extraOptions": [
                    {
                        "name": "Mayonnaise",
                        "selected": false,
                        "value": "2.10"
                    },
                    {
                        "name": "Tomato Sauce",
                        "selected": false,
                        "value": "3.20"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/664174cd-240d-2b41-f8e3-ee51a9380881-menu-item4-1.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F664174cd-240d-2b41-f8e3-ee51a9380881-menu-item4-1.jpeg?alt=media&token=e82d12a2-a9ea-49ae-8f8c-47a9df193f3f"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Single",
                        "value": "6"
                    },
                    {
                        "currency": "$",
                        "name": "Double",
                        "value": "11"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Celery",
                        "selected": true
                    },
                    {
                        "name": "Corn",
                        "selected": true
                    },
                    {
                        "name": "Cucumber",
                        "selected": true
                    },
                    {
                        "name": "Rice",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/88c813d5-e54d-7840-4054-4d0647a3a4b7-menu-item4-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F88c813d5-e54d-7840-4054-4d0647a3a4b7-menu-item4-thumb.png?alt=media&token=1365431e-91cf-4344-b7ab-867c849a0fef"
                    }
                ],
                "title": "Shrimp Rolls"
            },
            {
                "body": "Curabitur magna lectus, ullamcorper sed molestie in, facilisis sed libero. Morbi eleifend quam nunc, nec porttitor arcu varius a. Aliquam id dolor sem.",
                "category": "Desserts",
                "extraOptions": [
                    {
                        "name": "Sparkling Wine",
                        "selected": false,
                        "value": "7"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/8d772e51-ac25-8983-7cad-93e3ad37aac2-menu-item3-1.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F8d772e51-ac25-8983-7cad-93e3ad37aac2-menu-item3-1.jpeg?alt=media&token=7287c808-a748-4342-890b-049ef1dbad34"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Small",
                        "value": "6"
                    },
                    {
                        "currency": "$",
                        "name": "Normal",
                        "value": "9"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Chocolate Sauce",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/5c200198-1188-e6df-1822-c8b45cf90c34-menu-item3-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F5c200198-1188-e6df-1822-c8b45cf90c34-menu-item3-thumb.png?alt=media&token=bd3fd5a5-dac7-403b-98f7-7f5821843c4f"
                    }
                ],
                "title": "Plum Crepe"
            },
            {
                "body": "Mauris blandit odio eu ullamcorper vestibulum. Integer neque tortor, pretium at lectus sit amet, viverra dignissim felis.",
                "category": "Desserts",
                "extraOptions": [
                    {
                        "name": "Fresh Orange",
                        "selected": false,
                        "value": "2.90"
                    },
                    {
                        "name": "Sparkling Wine",
                        "selected": false,
                        "value": "7.50"
                    }
                ],
                "isFeatured": true,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/43990e86-7991-aae3-92c1-153d3b34df9a-menu-item6-1.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F43990e86-7991-aae3-92c1-153d3b34df9a-menu-item6-1.jpeg?alt=media&token=5a0a3254-7d57-4038-bdb6-766158e2cf95"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Small",
                        "value": "7"
                    },
                    {
                        "currency": "$",
                        "name": "Standard",
                        "value": "8.50"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Chocolate Syrup",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/41402bb4-bb38-3c9a-4f54-0a9624ee4756-menu-item6-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F41402bb4-bb38-3c9a-4f54-0a9624ee4756-menu-item6-thumb.png?alt=media&token=0c4ac4c0-4d67-4aae-93fd-aee56d5b7082"
                    }
                ],
                "title": "Fruit Tarte"
            },
            {
                "body": "Mauris sagittis rhoncus justo, vel bibendum nibh ultrices iaculis. Pellentesque ac facilisis mauris. Donec a tortor non ante pharetra pharetra nec ac augue. Nulla mollis lacinia est.",
                "category": "Main Course",
                "extraOptions": [
                    {
                        "name": "Cheddar",
                        "selected": false,
                        "value": "3.80"
                    },
                    {
                        "name": "Raspberry hot sauce",
                        "selected": false,
                        "value": "4"
                    }
                ],
                "isFeatured": false,
                "pictures": [
                    {
                        "inProgress": false,
                        "path": "files/7391c296-5b50-616f-a48a-068c1d571e12-menu-item5-1.jpeg",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2F7391c296-5b50-616f-a48a-068c1d571e12-menu-item5-1.jpeg?alt=media&token=37110d49-f139-4401-9cbf-db867684b146"
                    }
                ],
                "price": [
                    {
                        "currency": "$",
                        "name": "Single",
                        "value": "8.20"
                    },
                    {
                        "currency": "$",
                        "name": "Double",
                        "value": "13.50"
                    }
                ],
                "standardOptions": [
                    {
                        "name": "Tomatoes",
                        "selected": true
                    },
                    {
                        "name": "Red pepper jelly",
                        "selected": true
                    }
                ],
                "thumb": [
                    {
                        "inProgress": false,
                        "path": "files/e63bec50-b22e-5db1-992f-999781983de3-menu-item5-thumb.png",
                        "url": "https://firebasestorage.googleapis.com/v0/b/scigen-portal.appspot.com/o/files%2Fe63bec50-b22e-5db1-992f-999781983de3-menu-item5-thumb.png?alt=media&token=030fde08-526b-4e1f-8557-806c937b21be"
                    }
                ],
                "title": "Turkey Bites"
            }
        ]
		return this.menuList;
	};

    getEvents(){
        this.events = [
            {
                "deletable": false,
                "editable": false,
                "endsAt": 1505932200000,
                "incrementsBadgeTotal": false,
                "startsAt": 1505834129000,
                "title": "Hartford United States XL Center",
                "type": "info",
                "start": "2017-09-19T15:15:29.000Z",
                "end": "2017-09-20T18:30:00.000Z",
                "color": {
                    "primary": "#20ff60"
                }
            },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1508943600000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1508925600000,
                    "title": "Trenton Sun National Bank Center",
                    "type": "important",
                    "start": "2017-10-25T10:00:00.000Z",
                    "end": "2017-10-25T15:00:00.000Z",
                    "color": {
                        "primary": "#ffd50a"
                    }
                },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1434056400000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1433883600000,
                    "title": "Cincinnati US Bank Arena",
                    "type": "success",
                    "start": "2015-06-09T21:00:00.000Z",
                    "end": "2015-06-11T21:00:00.000Z",
                    "color": {
                        "primary": "#1e90ff"
                    }
                },
                {
                    "deletable": false,
                    "editable": false,
                    "endsAt": 1434402000000,
                    "incrementsBadgeTotal": false,
                    "startsAt": 1434229200000,
                    "title": "Milwaukee Marcus Amphitheater",
                    "type": "special",
                    "start": "2015-06-13T21:00:00.000Z",
                    "end": "2015-06-15T21:00:00.000Z",
                    "color": {
                        "primary": "#0008ff"
                    }
                }
            ];
        return this.events;

    };
}