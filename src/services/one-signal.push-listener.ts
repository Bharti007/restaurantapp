// import { Injectable } from '@angular/core';
// import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
// import { oneSignalSettings, sender_id } from '../config';
//
// @Injectable()
// export class OneSignalPushListener {
// 	constructor(private oneSignal: OneSignal) {
// 	}
//
// 	init() {
// 		if ((<any>window).cordova) {
// 			this.oneSignal.startInit(oneSignalSettings.appId, sender_id);
// 			this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
// 			this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data.payload));
// 			this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data.notification.payload));
// 			this.oneSignal.endInit();
// 		} else {
// 			console.log('OneSignal. Cordova is not available');
// 		}
// 	}
//
// 	private onPushReceived(payload: OSNotificationPayload) {
// 		alert('ONE_SIGNAL. Push recevied:' + payload.body);
// 	}
//
// 	private onPushOpened(payload: OSNotificationPayload) {
// 		alert('ONE_SIGNAL. Push opened: ' + payload.body);
// 	}
// }