import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TilesComponent } from './tiles/tiles.component';
@NgModule({
	declarations: [TilesComponent],
	
	imports: [IonicModule],
	exports: [TilesComponent]
})
export class CustomComponentsModule {
}